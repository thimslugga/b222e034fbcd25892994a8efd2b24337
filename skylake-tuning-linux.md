**Tuning Intel Skylake and beyond for optimal performance and feature level support on Linux:**

Note that on Skylake, Kabylake (and the now cancelled "Broxton") SKUs, functionality such as power saving, GPU scheduling and HDMI audio have been moved onto [binary-only firmware](https://01.org/linuxgraphics/downloads/firmware), and as such, the GuC and the HuC blobs must be loaded at run-time to access this functionality. 

Enabling GuC and HuC on Skylake and above requires a few extra parameters be passed to the kernel before boot. 

**Instructions provided for both Fedora and Ubuntu (including Debian):**


Note that the firmware for these GPUs is often packaged by your distributor, and as such, you can confirm the firmware blob's availability by running:

**1. On Fedora:**

    rpm -ql linux-firmware | fgrep i915

Sample output:

    /usr/lib/firmware/i915 
    /usr/lib/firmware/i915/bxt_dmc_ver1_07.bin 
    /usr/lib/firmware/i915/kbl_dmc_ver1.bin
    /usr/lib/firmware/i915/kbl_dmc_ver1_01.bin 
    /usr/lib/firmware/i915/skl_dmc_ver1_23.bin 
    /usr/lib/firmware/i915/skl_guc_ver1.bin 
    /usr/lib/firmware/i915/skl_guc_ver4.bin 
    /usr/lib/firmware/i915/skl_guc_ver6.bin 
    /usr/lib/firmware/i915/skl_guc_ver6_1.bin 
    /usr/share/doc/linux-firmware/LICENSE.i915

**2. On Ubuntu:**

    dpkg -L linux-firmware | fgrep i915

It may be wise to install the `linux-firmware-nonfree` package as it may contain extra firmware blobs on Ubuntu releases *prior to 16.04LTS*. Don't install this on 16.04LTS and above.

Now, to the real meat:

**GUC loading on Skylake and above:**

Add this to the kernel boot parameters on `/etc/default/grub`:

    intel_pstate=skylake_hwp 
    
    
The `intel_pstate=skylake_hwp option` enables Intel's Hardware P-States, known commonly by the marketing moniker "SpeedShift".
Note that Intel's IOMMU for VT-d (enabled in the kernel by passing `intel_iommu=on` to the kernel boot options) cannot be used together with frame buffer compression (`i915.enable_fbc=1` argument passed to the i915). Frame Buffer Compression will be disabled automatically to prevent screen tearing in this case. 

**Note:** The intel_pstate driver is the default since Linux 4.10 on SKL+. Thanks for the updates on this @pcordes and the correction on module options syntax in `/etc/modprobe.d/i915.conf`, @vinzent. Gr33ts ;-) 

Then, update grub on Fedora:

(Run commands as root):

For [EFI boot](https://wiki.archlinux.org/index.php/Unified_Extensible_Firmware_Interface) (More common):


    grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg


And if you're still booting up in [legacy BIOS mode on an MBR-style partitioning scheme](https://wiki.archlinux.org/index.php/partitioning#Master_Boot_Record) or with [CSM enabled](http://www.rodsbooks.com/efi-bootloaders/csm-good-bad-ugly.html) on Fedora for whatever reason:


    grub2-mkconfig -o /boot/grub2/grub.cfg
    
    
Then rebuild initramfs:

**On Fedora**:

    dracut --force


**On Debian-based distributions:**

Simply run:

    sudo update-initramfs

    update-grub

Then reboot.

Your modern Intel HD Graphics processor graphics will work just fine.

You can also add this to: `/etc/modprobe.d/i915.conf`

In the following syntax:

    options i915 enable_guc=3

Other safe options to pass are `enable_rc6=1`, `enable_fbc=1`, `enable_psr=1`, `disable_power_well=0` and `semaphores=1`

**Verify**

    cat /sys/kernel/debug/dri/0/i915_huc_load_status
    cat /sys/kernel/debug/dri/0/i915_guc_load_status


**Reference:**

See modinfo output for i915 for available GuC options:


    modinfo i915 | grep guc


**Further notes:**

A list of all options along with short descriptions and default values can be generated with the following command:

    $ modinfo -p i915

To check which options are currently enabled, run:

    # systool -m i915 -av

You will note that many options default to -1, resulting in per-chip power-saving defaults. It is however possible to configure more aggressive powersaving by using module options.
Warning: Diverting from the defaults will mark the kernel as tainted from Linux 3.18 onwards. This basically implies using other options than the per-chip defaults is considered experimental and not supported by the developers.

The following set of options should be generally safe to enable:

    /etc/modprobe.d/i915.conf
    
    options i915 enable_dc=1 enable_fbc=1 semaphores=1 enable_guc=3
    
On Linux 4.16+, GuC firmware loading and submission is now handled by the `enable_guc` parameter in place of the former `enable_guc_loading=1` and `enable_guc_submission=1` parameters. Adjust as needed.

**RC6 sleep modes (`enable_rc6`):**

You can experiment with higher values for enable_rc6, but your GPU may not support them or the activation of the other options:

The available `enable_dc` values are a bitmask with bit values `RC6=1`, `RC6p=2`, `RC6pp=4[4]` - where "`RC6p`" and "`RC6pp`" are lower power states.

To confirm the current running RC6 level, you can look in sysfs:

    # cat /sys/class/drm/card0/power/rc6_enable

If the value read is a lower number than expected, the other RC6 level are probably not supported. Passing `drm.debug=0xe` to the kernel boot options will add DRM debugging information to the kernel log - possibly including a line like this:

    [drm:sanitize_rc6_option] Adjusting RC6 mask to 1 (requested 7, valid 1)

**Framebuffer compression (`enable_fbc`):**

Framebuffer compression may be unreliable or unavailable on Intel GPU generations before Sandy Bridge (generation 6). This results in messages logged to the system journal similar to this one:

    kernel: drm: not enough stolen space for compressed buffer, disabling.


**Tear-free video:**

With the SNA acceleration method enabled, tearing may be observed. To fix this, enable the "`TearFree`" option in the driver by adding the following line to your xorg.conf (or ideally, a sub-configuration file under xorg.conf.d) configuration file:

    Option "TearFree" "true"
    
    
 This is not needed on server-grade SKUs where Xorg is unwelcome. 
    
    
**Debugging:**


Simply inspect dmesg:

    dmesg | grep drm

And also look at the output of:

    journalctl -b -o short-monotonic -k
    
    
**Screen corruption observed when waking up from suspend**

This is often observed as font and screen corruption in GTK+ applications (missing glyphs after suspend/resume).

Should you experience missing font glyphs in GTK+ applications, the following workaround might help. Edit `/etc/environment` to add the following line:

    /etc/environment
    COGL_ATLAS_DEFAULT_BLIT_MODE=framebuffer

See [this bug](https://bugs.freedesktop.org/show_bug.cgi?id=88584) here for more details.






